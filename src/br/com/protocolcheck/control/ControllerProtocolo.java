package br.com.protocolcheck.control;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;




@WebServlet("/ControllerProtocolo")
public class ControllerProtocolo extends HttpServlet {
	private static final long serialVersionUID = 1L;

	 String numeroprotocolo = "2475131".trim();

	public ControllerProtocolo() {

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String entrada = request.getParameter("numeroprotocolo");

		if (entrada.equals(numeroprotocolo)) {

			response.sendRedirect("protocoloresp.jsp");

		} else {

			response.sendRedirect("protocolo.jsp");

		}
	}
}
