<%@ page language="java" contentType="text/html; charset=ISO-8859-1"  pageEncoding="ISO-8859-1"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
 
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Protocolo</title>
</head>
<body>
<br>
<h2 align="center">Dados do Protocolo</h2>

<hr/>

<form method="get" action="ControllerProtocoloResp" align="center">
<ul>
<c:forEach items="${lista}" var="linha">
<li> ${linha.protocolo}, ${linha.sistema}, ${linha.valido}</li>
</c:forEach>
</ul>
<br/>

<br/>
<br/>

</form>

</body>
</html>